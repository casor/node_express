var jwt = require('jsonwebtoken');

module.exports = async (req, res, next) => {

    let token = req.headers['token'];//obtenemos el token que tenemos que mandar en las cabeceras cuando hacemos la petición a la API

    if (!token) {//si no se manda el token, entonces no lo dejamos acceder a la API
        res.send(401);
    }else{
        //primer parametro es para el token que estamos recibiendo
        //el segundo parametro es la llave para desencriptar
        jwt.verify(token, process.env.JWT_KEY, (err, decoded) => {

            if(err){//en caso de que el token sea invalido y/o exte expirado
                res.send(401)
            }else{
                next();//le permitimos el acceso a la API
            }
        })
    }
}