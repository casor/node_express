//importamos las librerias de express
const express = require('express');
const http = require('http');
const bodyParser = require('body-parser');

//inicializamos el Server
const app = express();
const server = http.Server(app);
const port = 9000;

require('dotenv').config() //se incluye el archivo env

app.use(bodyParser.json());

//definimos nuestra ruta
//para acceder a ella sería http://localhost:9000/
// app.get('/', function (req, res) {
//     res.send('Hola mundo con nodemon')
// })

//importamos nuestras rutas
const accountRoute = require('./routes/account');
const signinRoute = require('./routes/signin');

//inicializamos las rutas en la aplicación
app.use('/', accountRoute);
app.use('/', signinRoute);

//arrancamos el server
server.listen(port, () => {
    console.log(`Servidor online en el puerto: ${port}`);
});

