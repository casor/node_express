const Sequelize = require('sequelize');
//nombre de la base de datos, usuario, password, datos extras (host, port, tipo de base de datos(mysql, postgress, msql, etc))
const sequelize = new Sequelize(process.env.DB_NAME, process.env.DB_USER, process.env.DB_PASS, {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    dialect: 'mysql'
});

module.exports = sequelize;