const express = require('express');
const router = express.Router();
const sequelize = require('../models/connect');
const DataTypes = require('sequelize/lib/data-types');
const Cryptr = require('cryptr');
const cryptr = new Cryptr(process.env.CRYPTRKEY);//llave para encriptar/desencriptar. Lo ocuparemos para el password
const jwtAuth = require('../middleware/jwtAuth');
//importamos el modelo Account
const Accounts = require('../models/account')(sequelize, DataTypes);

// let accounts = [
//     {
//         id:1,
//         name:"Raul",
//         last_name:"Castro"
//     }
// ];

//obtiene todas las cuentas
router.get('/account', jwtAuth, async (req, res) => {

    Accounts.findAll()
    .then(accounts => {
        res.json(accounts);
    })
    //res.json(accounts);
})

//obtiene una cuenta en especifico
router.get('/account/:id', jwtAuth, async (req, res) => {

    let id = req.params.id;//obtenemos el id del usuario que se manda como parametro
    
    Accounts.findOne({
        where:{
            id: id
        }
    })
    .then(account => {
        if(account){
            res.json(account);
        }else{
            res.status(400).send({msg:"Usuario invalido"});
        }
    })
    // let index = accounts.map(function(account){
        
    //     return account.id;
    // }).indexOf(parseInt(id));//comparamos el id con el array donde se encuentran todas las cuentas
    // //si el id concuerda con alguno de los que se encuentran en el array
    // //nos regresa la posición

    // if(index === -1) {//sino existe, indicamos que la cuenta no existe
    //     res.status(400).send("La cuenta no existe");
    // }else{
    //     res.json(accounts[index]);//si existe, regresamos el registro que se busca
    // }
    
})

//agrega una nueva cuenta
router.post('/account', async (req, res) => {

    const { name, last_name, password } = req.body;

    Accounts.create({
        name:name,
        last_name:last_name,
        password: cryptr.encrypt(password)
    })
    .then(account => {
        if(account){
            res.send(account);
        }else{
            res.status(400).send({msg:"Ocurrio un problema al registrar"});
        }
    });
    // let id = accounts.length + 1;

    // accounts.push({
    //     id: id,
    //     name: name,
    //     last_name: last_name
    // });

    //res.send('Cuenta creada');
})

//actualiza una cuenta en especifico
router.put('/account/:id', async (req, res) => {

    const { name, last_name } = req.body;

    let id = req.params.id;//obtenemos el id del usuario que se manda como parametro
    
    Accounts.findOne({
        where:{
            id: id
        }
    })
    .then(account => {
        if(account){

            account.update({
                name:name,
                last_name: last_name
            });
            res.send({msg:"Usuario actualizado"});

        }else{
            res.status(400).send({msg:"Usuario invalido"});
        }
    })
    // let index = accounts.map(function(account){
        
    //     return account.id;
    // }).indexOf(parseInt(id));

    // if(index === -1) {//sino existe, indicamos que la cuenta no existe
    //     res.status(400).send("La cuenta no existe");
    // }else{
    //     accounts[index].name = name;
    //     accounts[index].last_name = last_name;

    //     res.json("Cuenta actualizada");
    // }
})

//elimina una cuenta en especifico
router.delete('/account/:id', async (req, res) => {

    let id = req.params.id;//obtenemos el id del usuario que se manda como parametro
    Accounts.destroy({
        where:{
            id: id
        }
    })
    .then(account => {
        res.send({msg:"Usuario eliminado"});
    });
    // let index = accounts.map(function(account){
        
    //     return account.id;
    // }).indexOf(parseInt(id));

    // if(index === -1) {//sino existe, indicamos que la cuenta no existe
    //     res.status(400).send("La cuenta no existe");
    // }else{
    //     accounts.splice(index, 1);

    //     res.json("Cuenta eliminada");
    // }
})

module.exports = router;