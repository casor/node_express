const express = require('express');
const router = express.Router();
const sequelize = require('../models/connect');
const DataTypes = require('sequelize/lib/data-types');
const Cryptr = require('cryptr');
const cryptr = new Cryptr(process.env.CRYPTRKEY);
var jwt = require('jsonwebtoken');

//importamos el modelo Account
const Accounts = require('../models/account')(sequelize, DataTypes);

//iniciamos sesión
router.post('/signin', async (req, res) => {

    const { name, password } = req.body

    Accounts.findOne({
        where:{
            name: name
        }
    })
    .then(account => {
        if (!account) {
            res.status(401).send({msg:"Usuario y/o contraseña invalido"});
        }else{

            let pass = cryptr.decrypt(account.password);//desencriptamos la contraseña que esta en la base de datos

            if (pass != password) {
                res.status(401).send({msg:"Usuario y/o contraseña invalido"});
            }else{
                //en el primer parametro, ingresamos la información que queremos
                //encriptar, en el segundo parametro la llave
                let token = jwt.sign({id:account.id}, process.env.JWT_KEY);
                res.json({token:token});//retornamos solamente el token y es el que 
                //usaremos para acceder a nuestra API
            }
        }
    });
})

module.exports = router